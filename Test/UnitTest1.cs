using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SlpBsLogAnalyzer;
using SlpBsLogAnalyzer.Messages;
using System.Collections.Generic;
using System.IO;
using System.Net.WebSockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks.Dataflow;

namespace Test
{

    /*[TestClass]
    public class UnitTest1
    {
        private static void Write(string a, string b)
        {
            var sb = new StringBuilder();
            foreach (char symb in a)
            {
                if (symb == '\t')
                    sb.Append("ttttttt");
                else if (symb == '\r')
                    sb.Append("\\r");
                else if (symb == '\n')
                    sb.Append("\\n\n");
                else
                    sb.Append(symb);
            }

            using (var sw = new StreamWriter(new FileStream(ExpectedResult, FileMode.Create, FileAccess.Write)))
                sw.Write(sb.ToString());

            sb.Clear();

            foreach (char symb in b)
            {
                if (symb == '\r')
                    sb.Append("\\r");
                else if (symb == '\t')
                    sb.Append("ttttttt");
                else if (symb == '\n')
                    sb.Append("\\n\n");
                else
                    sb.Append(symb);
            }
            sb.Append($"{a.Length}/{b.Length}\n");
            using (var sw = new StreamWriter(new FileStream(Actual, FileMode.Create, FileAccess.Write)))
                sw.Write(sb.ToString());
        }

        private static string SmallMessage => File.ReadAllText(@"C:\Users\24012\Desktop\Message samples\small.txt");

        private static string ErrorMessage => File.ReadAllText(@"C:\Users\24012\Desktop\Message samples\error.txt");

        private static string ObjectMessage => File.ReadAllText(@"C:\Users\24012\Desktop\Message samples\object.txt");

        private static string DeviceMessage => File.ReadAllText(@"C:\Users\24012\Desktop\Message samples\device.txt");

        private static string SANRMessage => File.ReadAllText(@"C:\Users\24012\Desktop\Message samples\STARTANEWWHAT.txt");

        private static string Parametres => File.ReadAllText(@"C:\Users\24012\Desktop\Message samples\bigparametresdata.txt");

        private static string Telemetry => File.ReadAllText(@"C:\Users\24012\Desktop\Message samples\Telemetry.txt");

        private static string NewProblem => File.ReadAllText(@"C:\Users\24012\Desktop\Message samples\newproblem.txt");

        private static string NewDevice => File.ReadAllText(@"C:\Users\24012\Desktop\Message samples\newdevice.txt");

        private static string FullLog => File.ReadAllText(@"C:\Users\24012\Desktop\slp logs\slp_log_397.log");

        private static string FullLogPath => @"C:\Users\24012\Desktop\slp logs\slp_log_397.log";

        private static Regex SplitNumber => new Regex(@"_\d\d\d\.");

        private static string ExpectedResult => @"C:\Users\24012\Desktop\expected.txt";

        private static string Actual => @"C:\Users\24012\Desktop\actual.txt";

        private static string Mistakes => @"C:\Users\24012\Desktop\mistakes.txt";

        private bool IsOk()
        {
            var sb = new StringBuilder();
            for (int i = 3962; i <= 3962; i++)
            {
                var twolines = SplitNumber.Split(FullLogPath);
                var log = File.ReadAllText($"{twolines[0]}_{i}.{twolines[1]}");
                sb.Clear();
                foreach (IMessage message in TextParser.ParseToMessageArray(log))
                {
                    if (message as SlpBsLogAnalyzer.Messages.Message != null)
                        sb.Append($"{message}\n\n");
                    else
                    {
                        sb.Append($"{message}\n");
                    }
                }
                if (!log.Replace("\r\n", "\r").Equals(sb.ToString()))
                {
                    Write(log.Replace("\r\n", "\r"), sb.ToString());
                    var one = log.Replace("\r\n", "\r").Split('\n');
                    var two = sb.ToString().Split('\n');
                    var max = two.Length;
                    if (one.Length < two.Length)
                        max = one.Length;
                    var sb1 = new StringBuilder();
                    sb1.Append($"{i}\n\n");
                    for (var j = 0; j < max; j++)
                    {
                        if (!one[j].Equals(two[j]))
                            sb1.Append($"{one[j]}\n{two[j]}\n{j}=========================\n");
                    }

                    using (var sw = new StreamWriter(new FileStream(Mistakes, FileMode.Create, FileAccess.Write)))
                        sw.Write(sb1.ToString());
                    return false;
                }
            }
            return true;
        }

        [TestMethod]

        // ��������� � ���� ������.

        public void _1()
        {
            var small = TextParser.ParseToMessageArray(SmallMessage)[0];
            Assert.AreEqual(SmallMessage, small.ToString());
        }

        [TestMethod]

        // ��������� � ��� ������ � �������.

        public void _2()
        {
            var error = TextParser.ParseToMessageArray(ErrorMessage)[0];
            Assert.AreEqual(ErrorMessage, error.ToString().Replace("\n", "\r\n"));
        }

        [TestMethod]

        // ��������� � ��������.

        public void _3()
        {
            var obj = TextParser.ParseToMessageArray(ObjectMessage)[0];
            Assert.AreEqual(ObjectMessage, obj.ToString().Replace("\n", "\r\n"));
        }

        [TestMethod]

        // ��������� � ����������� �� ����������.

        public void _4()
        {
            var obj = TextParser.ParseToMessageArray(DeviceMessage);
            Assert.AreEqual(DeviceMessage, obj[0].ToString().Replace("\n", "\r\n"));
        }

        [TestMethod]

        // .

        public void _5()
        {
            //Write(Parametres+"\r\n", TextParser.ParseToMessageArray(Parametres)[0].ToString().Replace("\n", "\r\n"));
            Assert.AreEqual(Parametres.Replace("\t", "       "), TextParser.ParseToMessageArray(Parametres)[0].ToString().Replace("\n", "\r\n"));
        }

        [TestMethod]

        // Start a new Record.

        public void _6()
        {
            var obj = TextParser.ParseToMessageArray(SANRMessage)[0];
            Assert.AreEqual(SANRMessage.Split('\r')[0], obj.ToString().Replace("\n", "\r\n"));
        }

        [TestMethod]

        public void _7()
        {
            var obj = TextParser.ParseToMessageArray(Telemetry)[0];

            Assert.AreEqual(Telemetry, obj.ToString().Replace("\n", "\r\n"));
        }

        [TestMethod]

        public void _8()
        {
            Assert.AreEqual(NewProblem, TextParser.ParseToMessageArray(NewProblem)[0].ToString().Replace("\n", "\r\n"));
        }

        [TestMethod]

        public void _9()
        {
            var mes = TextParser.ParseToMessageArray(NewDevice)[0];
            Assert.AreEqual(NewDevice, mes.ToString().Replace("\n", "\r\n"));
        }

        //[TestMethod]

        *//*public void _A()
        {

            //Assert.AreEqual(new KeyValuePair<string, string>("Snr", "33"), (KeyValuePair<string, string>)TextParser.ParseToMessageArray(NewProblem)[0].Data.Search("Snr"));
           Assert.IsTrue(IsOk());
        }*//*

        [TestMethod]

        public void _B()
        {
            var array = TextParser.ParseToMessageArray(FullLog);
            dynamic obj = false;
            foreach (IMessage message in array)
            {
                try
                {
                    obj = message.Data.Search("su", typeof(MessageObject));
                    bool a = obj;
                }
                catch
                {
                    break;
                }
            }
            Assert.IsTrue(obj as MessageObject != null);
        }
}*/

}
