﻿using Avalonia.Media;
using ReactiveUI;
using SlpBsLogAnalyzer.Messages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading;

namespace SlpBsLogAnalyzer.Painters
{
    public class VisualizedMessage
    {
        private static int DateTimeLength => 23;

        private static int WarningLength => 7;

        private static int ErrorLength => 5;

        private static int CriticalLength => 8;

        public int Length => _message.ToString().Length;

        public bool IsMessage => _message as Message != null;

        public int LineCount => _message.LineCount;

        private readonly IMessage _message;

        private int _startindex = 0;

        private int _line;

        public Colorizer[] Colorizers { get; private set; }

        public VisualizedMessage(IMessage message) => _message = message;

        private void Add(List<Colorizer> colorizers, int length, IBrush color)
        {
            colorizers.Add(new Colorizer(_line, ++_startindex, length, color));
            _startindex += length;
        }

        public void SetColorizer(int lineNumber, int startind)
        {
            _line = lineNumber;

            _startindex = startind;

            IBrush color;

            var colorizers = new List<Colorizer>();

            int typelength;

            if (_message as Message != null)
            {
                Add(colorizers, DateTimeLength, Colors.DateTime);

                if (_message.Type.Equals("warning") || _message.Type.Equals("error") || _message.Type.Equals("critical"))
                {
                    if (_message.Type.Equals("warning"))
                    {
                        color = Colors.Warning;
                        typelength = WarningLength;
                    }
                    else if (_message.Type.Equals("error"))
                    {
                        typelength = ErrorLength;
                        color = Colors.Error;
                    }
                    else
                    {
                        typelength = CriticalLength;
                        color = Colors.Critical;
                    }

                    _startindex += 2;

                    Add(colorizers, typelength, color);
                    Add(colorizers, _message.Data.Value.Length - 1, color);

                    _line++;
                    for (var i = 0; i < _message.Data.Children.Count; i++)
                    {
                        Add(colorizers, _message[i].Value.Length - 1, color);
                        _line++;
                    }
                }
                else if (_message.Type.Equals(IMessage.WithoutType))
                {

                    _startindex += _message.Data.Value.Length;
                    _line++;
                    for (var i = 0; i < _message.Data.Children.Count; i++)
                    {
                        if (_message.Data[i].IsKeyValuePair)
                        {
                            _startindex++;

                            // + 1 у KeyLength, чтобы покрасить знак ':'.

                            Add(colorizers, _message.Data[i].KeyLength + 1, Colors.Key);

                            // + 1 у ValueLength, для учёта пробела.

                            _startindex += _message.Data[i].ValueLength + 1;
                            _line++;
                        }
                        else if (_message[i].IsThereDevice)
                        {
                            var array = _message.Data[i].Value.Split(':');

                            Add(colorizers, array[0].Length + 1, Colors.Key);
                            _startindex -= 2;
                            for (var j = 1; j < array.Length; j++)
                                _startindex += array[j].Length + 1;
                            _line++;
                        }
                        else if (_message[i].Value.Contains(": {"))
                        {
                            var array = _message[i].Value.Split(':');
                            int length = array[0].Length + 1;
                            Add(colorizers, length, Colors.Key);
                            _line++;
                            for (var j = 1; j < array.Length; j++)
                            {
                                _startindex += array[j].Length + 1; 
                            }
                        }
                        else
                        {
                            _startindex += _message.Data[i].Value.Length;
                            _line += _message.Data[i].Value.Split('\n').Length;
                        }
                    }

                }
            }
            else if (_message as SANR != null)
            {

            }
            else if (_message as Parametres != null)
            {
                _startindex += Parametres.DefaultStart.Length;
                var array = _message.ToString().Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                for (var i = 1; i < array.Length - 1; i++)
                {
                    _line++;
                    Add(colorizers, array[i].Length, Colors.Parametres);
                }
            }
            Colorizers = colorizers.ToArray();
        }

        public override string ToString() => _message.ToString();

    }
}
