﻿using Avalonia.Media;
using AvaloniaEdit.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace SlpBsLogAnalyzer.Painters
{

    /// <summary>
    /// 
    /// </summary>

    public class Colorizer : DocumentColorizingTransformer
    {

        private readonly IBrush _colour;
        private readonly int _startind;
        private readonly int _length;
        private readonly int _lineNumber;

        public Colorizer(int lineNumber, int startind, int length, IBrush colour)
        {
            _lineNumber = lineNumber;
            _colour = colour;
            _length = length;
            _startind = startind;
        }

        protected override void ColorizeLine(AvaloniaEdit.Document.DocumentLine line)
        {
            if (!line.IsDeleted && line.LineNumber == _lineNumber)
            {
#if !DEBUG
                try
                {
                    ChangeLinePart(_startind, _startind + _length, ApplyChanges);
                }
                catch
                { }
#else

                ChangeLinePart(_startind, _startind + _length, ApplyChanges);
#endif

            }

        }

        void ApplyChanges(VisualLineElement element)
        {
            element.TextRunProperties.ForegroundBrush = _colour;
        }

    }
}