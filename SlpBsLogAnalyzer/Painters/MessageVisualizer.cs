﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using DynamicData;
using DynamicData.Aggregation;
using SlpBsLogAnalyzer.Messages;
using SlpBsLogAnalyzer.Views;

namespace SlpBsLogAnalyzer.Painters
{
    public static class MessageVisualizer
    {

        private static double OneLineOffsetCount;

        public static int MessageCount => _messages.Count;

        private static AvaloniaEdit.TextEditor _textEditor;

        private static readonly List<VisualizedMessage> _messages = new List<VisualizedMessage>();

        public static int MaxCount
        {
            get
            {
                var mincount = _textEditor.ViewportHeight / OneLineOffsetCount / 2;
                if (_messages.Count < mincount || _messages.Count < AppSettings.Current.MessageCount)
                    return _messages.Count;
                if (AppSettings.Current.MessageCount > mincount)
                    return AppSettings.Current.MessageCount;
                return (int)Math.Ceiling(mincount);
            }
        }

        private static VisualizedMessage LastMessage => _messages[_endIndex];

        private static VisualizedMessage FirstMessage => _messages[_startIndex];

        private static double RangeToChangeMessage => 150d;

        private static int Count => _endIndex - _startIndex;

        private static double _lastVerticalOffset = 0;

        private static int _startIndex;

        private static int _endIndex;

        public static void AddRange(IMessage[] messages)
        {
            foreach (var a in messages)
            {
                _messages.Add(new VisualizedMessage(a));
            }
            if (MaxCount > Count)
                Avalonia.Threading.Dispatcher.UIThread.InvokeAsync(new Action(() =>
                {
                    while (MaxCount > Count)
                    {
                        DrawLast();
                    }
                }));
        }

        private static void DrawLast()
        {
            if (Count < MaxCount)
            {

                LastMessage.SetColorizer(_textEditor.LineCount, _textEditor.Text.Length);
                if (LastMessage.IsMessage)
                {
                    _textEditor.AppendText($"{LastMessage}\n\n");
                }
                else
                {
                    _textEditor.AppendText($"{LastMessage}\n");
                }
                _textEditor.TextArea.TextView.LineTransformers.AddRange(LastMessage.Colorizers);

                _endIndex++;
            }

        }

        public static void TextEditor_LayoutUpdated(object sender, EventArgs e)
        {
            if (_lastVerticalOffset != _textEditor.VerticalOffset)
            {
                if (_lastVerticalOffset < _textEditor.VerticalOffset)
                {
                    _lastVerticalOffset = _textEditor.HorizontalOffset;
                    if (_textEditor.ScrollViewer.Extent.Height - RangeToChangeMessage < _textEditor.VerticalOffset + _textEditor.ViewportHeight)
                    {
                        if (_endIndex < _messages.Count)
                        {
                            _startIndex++;
                            DrawLast();
                        }
                    }
                }
                else
                {
                    _lastVerticalOffset = _textEditor.HorizontalOffset;
                }

            }
        }

        public static void ClearAll()
        {
            _textEditor.Clear();
            _textEditor.TextArea.TextView.LineTransformers.Clear();
            _endIndex = 0;
            _messages.Clear();
            _startIndex = 0;
        }

        public static AvaloniaEdit.TextEditor TextEditor
        {
            set
            {
                if (_textEditor == null)
                    _textEditor = value;
                OneLineOffsetCount = value.FontSize;
            }
        }

        public static void Remove(bool deleteFirst)
        {
            if (_endIndex >= _startIndex)
            {
                if (deleteFirst)
                    SelectFirst();
                else
                    SelectLast();

                _textEditor.IsReadOnly = false;
                _textEditor.Cut();
                _textEditor.IsReadOnly = true;
            }

        }

        private static void SelectFirst()
        {
            _textEditor.SelectionStart = 0;
            _textEditor.SelectionLength = FirstMessage.Length + 1;
            if (FirstMessage.IsMessage)
                _textEditor.SelectionLength++;
            _startIndex++;
        }

        private static void SelectLast()
        {

            _textEditor.SelectionStart = _textEditor.Text.Length - LastMessage.Length - 1;
            if (LastMessage.IsMessage)
                _textEditor.SelectionStart--;
            _textEditor.SelectionLength = _textEditor.Text.Length - _textEditor.SelectionStart;
            //if (_messages.Count <= _endIndex)
            _endIndex--;

        }

    }
}
