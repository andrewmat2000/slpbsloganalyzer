﻿using Avalonia.Media;
using System;
using System.Collections.Generic;
using System.Text;

namespace SlpBsLogAnalyzer.Painters
{
    public static class Colors
    {
        public static  IBrush DateTime => Brush.Parse("#305FCA");

        public static  IBrush Key => Brush.Parse("#6C6C6C");

        public static  IBrush Error => Brush.Parse("#FF4040");

        public static  IBrush Critical => Brush.Parse("#FF0000");

        public static  IBrush Warning => Brush.Parse("#E15D31");

        public static  IBrush Parametres => Brush.Parse("#7FCA59");
    }
}
