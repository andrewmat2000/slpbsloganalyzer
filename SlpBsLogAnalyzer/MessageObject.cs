﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Text.RegularExpressions;

namespace SlpBsLogAnalyzer
{

    /// <summary>
    /// Класс - костыль. Для нестандартных объектов.
    /// </summary>

    public class MessageObject
    {
        public string Name { get; private set; }

        public Dictionary<string, string> Attributes { get; private set; }

        public int? Id
        {
            get
            {
                if(Attributes.ContainsKey("id"))
                {
                    if(int.TryParse(Attributes["id"], out int result))
                    {
                        return result;
                    }
                }
                return null;
            }
        }

        public string Type
        {
            get
            {
                if (Name.Contains("su"))
                    return "su";
                if (Name.Contains("Telemetry"))
                    return "Telemetry";
                if (Name.Contains("{") && Name.Contains("}"))
                    return "bare_data";
                return "unknown";
            }
        }

        public List<int> EndLinesIndexes { get; private set; }

        public MessageObject(string name)
        {
            Name = name;
            Attributes = new Dictionary<string, string>();
            EndLinesIndexes = new List<int>();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (!Type.Equals("bare_data"))
            { 
                if (Type.Equals("su"))
                    sb.Append($"\t{Name}: \n");
                sb.Append('\t');
                for (var i = 0; i < Attributes.Count; i++)
                {
                    sb.Append($"{Attributes.ElementAt(i).Key}: {Attributes.ElementAt(i).Value}");
                    if (EndLinesIndexes.Contains(i))
                    {
                        sb.Append("\n");
                        if (i != Attributes.Count - 1)
                        {
                            sb.Append('\t');
                        }
                    }
                    else
                    {
                        sb.Append("  -  ");
                    }
                }
            }
            else
            {
                sb.Append($"\t{Attributes.ElementAt(0).Key}: {{{Attributes.ElementAt(0).Value} }}\n");
            }
            return sb.ToString();
        }
    }
}
