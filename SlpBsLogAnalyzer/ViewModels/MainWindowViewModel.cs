﻿using Avalonia.Media;
using AvaloniaEdit.Rendering;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SlpBsLogAnalyzer.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public string MessageCount
        {
            get => _messageCount;
            set => this.RaiseAndSetIfChanged(ref _messageCount, value);
        }

        private string _messageCount = "0";

        public MainWindowViewModel(AppSettings appSettings)
        {
            AppSettings = appSettings;
        }

        public AppSettings AppSettings
        {
            set
            {
                FontSize = value.FontSize;
                FontFamily = new FontFamily(value.FontFamily);
            }
        }

        public FontFamily FontFamily
        {
            get => _fontFamily;
            set => this.RaiseAndSetIfChanged(ref _fontFamily, value);
        }

        private FontFamily _fontFamily;

        public double FontSize
        {
            set => this.RaiseAndSetIfChanged(ref _fontSize, value);
            get => _fontSize;
        }

        private double _fontSize;

    }
}

