﻿using System;
using Avalonia;
using System.Collections.Generic;
using System.Text;
using Avalonia.Controls;
using ReactiveUI;
using Avalonia.Markup.Xaml;
using System.Drawing;

namespace SlpBsLogAnalyzer.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {
        private static int MinimalMessageCount => 15;

        private static int MinimalFontSize => 7;

        public List<ComboBoxItem> Fonts
        {
            get => _fonts;
            set => this.RaiseAndSetIfChanged(ref _fonts, _fonts);
        }

        public ComboBoxItem SelectedFont
        {
            get => _selectedFont;
            set => this.RaiseAndSetIfChanged(ref _selectedFont, value);
        }

        private ComboBoxItem _selectedFont = new ComboBoxItem();

        private List<ComboBoxItem> _fonts = new List<ComboBoxItem>();

        public string FontSize
        {
            get => _settings.FontSize.ToString();

            set
            {
                try
                {
                    var size = double.Parse(value.Replace(".", ","));
                    if (size >= MinimalFontSize)
                        _settings.FontSize = size;
                    this.RaisePropertyChanged("FontSize");
                }
                catch { }
            }
        }

        public string MessageCount
        {
            get => _settings.MessageCount.ToString();
            set
            {
                try
                {
                    var count = int.Parse(value);
                    if (count >= MinimalMessageCount)
                        _settings.MessageCount = count;
                }
                catch { }
            }
        }

        public List<ComboBoxItem> Themes
        {
            get => new List<ComboBoxItem>
            {
                new ComboBoxItem{ Content = "Светлая" },
                new ComboBoxItem{ Content = "Тёмная"}
            };
        }

        public AppSettings AppSettings => _settings;

        private readonly AppSettings _settings;

        public SettingsViewModel(AppSettings settings)
        {
            _settings = settings;
            foreach (var font in FontFamily.Families)
            {
                _fonts.Add(new ComboBoxItem() { Name = font.Name, Content = font.Name });
            }
            _selectedFont = _fonts.Find((ComboBoxItem item) => item.Content.Equals(_settings.FontFamily));
        }


    }
}
