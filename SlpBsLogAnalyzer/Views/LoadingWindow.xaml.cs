using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using SlpBsLogAnalyzer.Styles;
using System.ComponentModel.Design;
using System.Threading;
using ReactiveUI;

namespace SlpBsLogAnalyzer.Views
{
    public class LoadingWindow : Window
    {
        internal class ViewModel : ReactiveObject
        {
            public IBrush GridBackground 
            {
                get => _gridForeground;
                set => this.RaiseAndSetIfChanged(ref _gridForeground, value);
            }

            private IBrush _gridForeground;

            public string Text
            {
                get => _text;
                set => this.RaiseAndSetIfChanged(ref _text, value, "Text");
            }

            private string _text = "";

            public double Value
            {
                get => _value;
                set => this.RaiseAndSetIfChanged(ref _value, value);
            }

            private double _value = 0;

            public double Maximum
            {
                get => _maximum;
                set => this.RaiseAndSetIfChanged(ref _maximum, value);
            }

            private double _maximum = 0;

            public double Minimum
            {
                get => _minimum;
                set => this.RaiseAndSetIfChanged(ref _minimum, value);
            }

            private double _minimum = 0;

        }

        private readonly ViewModel _viewModel = new ViewModel();

        private bool IsWorking = true;

        public IBrush GridBackground 
        {
            set => _viewModel.GridBackground = value;
        }

        private string Text
        {
            get => _viewModel.Text;
            set => _viewModel.Text = value;
        }

        public double Value
        {
            get => _viewModel.Value;
            set => _viewModel.Value = value;
        }

        public double Minimum
        {
            get => _viewModel.Minimum;
            set => _viewModel.Minimum = value;
        }

        public double Maximum
        {
            get => _viewModel.Maximum;
            set => _viewModel.Maximum = value;
        }

        private readonly StyleManager _styleManager;

        public LoadingWindow()
        {
            InitializeComponent();
            _styleManager = new StyleManager(this);
            _styleManager.UseTheme(StyleManager.CurrentTheme);
            StyleManager.ThemeChanged += StyleManager_ThemeChanged;
            new Thread(ThreadStart =>
            {
                while (IsWorking)
                {
                    Thread.Sleep(250);
                    switch (Text.Length)
                    {
                        case 0:
                        case 1:
                        case 2:
                            Text += ".";
                            break;
                        case 3:
                            Text = "";
                            break;
                    }
                }
            })
            { IsBackground = true }.Start();
        }

        private void StyleManager_ThemeChanged()
        {
            _styleManager.UseTheme(StyleManager.CurrentTheme);
        }

        public void LoadingWindow_Closed(object sender, System.EventArgs e)
        {
            IsWorking = false;
        }

        private void InitializeComponent()
        {
            DataContext = _viewModel;
            AvaloniaXamlLoader.Load(this);
        }
    }
}