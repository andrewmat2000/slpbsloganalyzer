using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using Avalonia.Threading;
using SkiaSharp;
using SlpBsLogAnalyzer.Styles;
using System;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SlpBsLogAnalyzer.Views
{

    /// <summary>
    /// ���� �������� ����������.
    /// </summary>

    public class SettingsWindow : Window
    {

        #region ����

        private readonly StyleManager _styleManager;

        private AppSettings _appSettings;

        private readonly ViewModels.SettingsViewModel _viewModel;

        private bool _isWorking = true;

        #endregion

        #region ��������

        private static Regex LetterSeparator => new Regex(@"[^0-9]{1,}");

        private ComboBox ComboBoxTheme => this.Find<ComboBox>("ComboBoxTheme");

        #endregion

        public SettingsWindow(AppSettings settings)
        {
            InitializeComponent();
            _viewModel = new ViewModels.SettingsViewModel(settings.Clone());
            DataContext = _viewModel;
            _styleManager = new StyleManager(this);
            _styleManager.UseTheme(settings.Theme);
            _appSettings = settings.Clone();
            Closing += (s, e) => _isWorking = false;
            ComboBoxTheme.SelectedIndex = (int)settings.Theme;
        }

        public void ComboBoxFontFamilies_KeyUp(object sender, Avalonia.Input.KeyEventArgs e)
        {
            if ((int)e.Key > 43 && (int)e.Key < 70)
            {
                var cb = (ComboBox)sender;
                var key = e.Key.ToString();
                if (key[0].CompareTo(_viewModel.AppSettings.FontFamily[0]) >= 0)
                {
                    cb.ScrollIntoView(_viewModel.Fonts.FindLast(
                        (ComboBoxItem item) => item.Content.ToString().StartsWith(key))
                        );
                }
                else
                {
                    cb.ScrollIntoView(_viewModel.Fonts.Find(
                        (ComboBoxItem item) => item.Content.ToString().StartsWith(key))
                        );
                }
            }
        }

        public void ComboBoxFontFamilies_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_appSettings != null)
                _viewModel.AppSettings.FontFamily = ((ComboBoxItem)e.AddedItems[0]).Content.ToString();
        }

        public async Task<AppSettings> GetSettingsAsync() => await Task.Run(() =>
        {
            while (_isWorking)
            {
                Thread.Sleep(150);
            }
            return _appSettings;
        });

        public void TextBoxMessageCount_LostFocus(object sender, Avalonia.Interactivity.RoutedEventArgs e)
        {
            var tb = (TextBox)sender;
            try
            {
                int count = Convert.ToInt32(tb.Text);
            }
            catch
            {
                var sb = new StringBuilder();
                foreach (string line in LetterSeparator.Split(tb.Text))
                {
                    sb.Append(line);
                }
                tb.Text = sb.ToString();
            }
        }

        public SettingsWindow() => InitializeComponent();

        public void ButtonExit_Click(object sender, Avalonia.Interactivity.RoutedEventArgs e) => Close();

        public void ButtonSave_Click(object sender, Avalonia.Interactivity.RoutedEventArgs e)
        {
            _viewModel.AppSettings.Theme = (Themes)ComboBoxTheme.SelectedIndex;
            _appSettings = _viewModel.AppSettings;
            Close();
        }

        public void ButtonDefault_Click(object sender, Avalonia.Interactivity.RoutedEventArgs e)
        {
            _appSettings = new AppSettings();
            Close();
        }

        private void InitializeComponent() => AvaloniaXamlLoader.Load(this);

    }
}