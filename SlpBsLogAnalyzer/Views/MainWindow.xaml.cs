using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Interactivity;
using Avalonia;
using Avalonia.Platform;
using AvaloniaEdit;
using System.Reflection.Metadata;
using Avalonia.Markup.Xaml.Templates;
using AvaloniaEdit.Rendering;
using Avalonia.Controls.Shapes;
using DynamicData.Kernel;
using System.Reflection;
using System.Drawing.Printing;
using DynamicData.Annotations;
using Avalonia.Utilities;
using ReactiveUI;
using System.Threading.Tasks.Sources;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net.Http;
using System.Text.Json;
using Avalonia.Styling;
using SlpBsLogAnalyzer.Styles;
using SlpBsLogAnalyzer.Messages;
using SlpBsLogAnalyzer.Painters;
using System.Diagnostics;
using System.ComponentModel.Design;
using System.Text.RegularExpressions;
using AvaloniaEdit.Editing;
using Avalonia.Threading;
using Avalonia.Media;
using System.Threading.Tasks;

namespace SlpBsLogAnalyzer.Views
{
    public class MainWindow : Window
    {

        #region ��������

        private void InitializeComponent() => AvaloniaXamlLoader.Load(this);

        /// <summary>
        /// ���������� x ��� ���� �� ������ ����.
        /// </summary>

        private int PositionLoadingWindowX => Position.X + ProgressBarHorizontalPixelOffset + (int)((Width - LoadingWindowWidth) / 2);

        /// <summary>
        /// ���������� y ��� ���� �� ������ ����.
        /// </summary>

        private int PositionLoadingWindowY => Position.Y + ProgressBarVerticalPixelOffset + ((int)(Height - LoadingWindowHeight) / 2);

        /// <summary>
        ///  ������ ��� ToString() ��� ������ ����.
        /// </summary>

        private static string DateFormat => "dd.MM.yyyy HH:mm:ss";

        public TextEditor TextEditor => this.Find<TextEditor>("textEditor");

        private TextBox TextBoxStartDate => this.Find<TextBox>("TextBoxStartDate");

        private TextBox TextBoxEndDate => this.Find<TextBox>("TextBoxEndDate");

        /// <summary>
        /// �������� ��������� ��������� ���� �������.
        /// </summary>

        private DateTime StartDate
        {
            get
            {
                var dt = GetDateTimeFromString(TextBoxStartDate.Text);
                if (dt == null)
                {
                    TextBoxStartDate.Text = MessageOutputManager.StartDate.ToString(DateFormat);
                    return MessageOutputManager.StartDate;
                }
                return dt.Value;
            }
        }

        /// <summary>
        /// �������� ��������� �������� ���� �������.
        /// </summary>

        private DateTime EndDate
        {
            get
            {
                var dt = GetDateTimeFromString(TextBoxEndDate.Text);
                if (dt == null)
                {
                    TextBoxStartDate.Text = MessageOutputManager.EndDate.ToString(DateFormat);
                    return MessageOutputManager.EndDate;
                }
                return dt.Value;
            }
        }

        private Window GetWindow() => (Window)VisualRoot;

        /// <summary>
        /// ����������� ������ ������������.
        /// </summary>

        private static int LoadingWindowHeight => 50;

        /// <summary>
        /// �������� ��� ��������� ������ ������������.
        /// </summary>

        private int LoadingWindowWidth => (int)Width - 600;

        /// <summary>
        /// ������-�� �������� ��� ��������� ���������� �� 8 �������� �����, ��� ��������� ������ �������� ��� �� �����.
        /// </summary>

        private int ProgressBarHorizontalPixelOffset => 8 + 70;

        private int ProgressBarVerticalPixelOffset => 10;

        private string SettingsPath => "properties.json";

        private static Regex IsDateTime => new Regex(@"((\d{1,2}))\.(\d{1,2})\.20[0-9][0-9]\s([0-9]{1,2}:){2}[0-9]{1,2}");

        private int MessagesCount
        {
            get => Convert.ToInt32(_viewModel.MessageCount);
            set => _viewModel.MessageCount = value.ToString();
        }

        private AppSettings Settings
        {
            get => AppSettings.Current;
            set
            {
                AppSettings.Current = value;
                _styleManager.UseTheme(value.Theme);
            }
        }

        #endregion

        #region ����

        private CancellationTokenSource _cancellationToken;

        private TextParser _textParser = new TextParser();

        private readonly StyleManager _styleManager;

        private static LoadingWindow _loadingWindow;

        private SettingsWindow _settingsWindow;

        private readonly ViewModels.MainWindowViewModel _viewModel;

        private AboutWindow _aboutWindow;

        #endregion

        private void WriteSettings()
        {
            try
            {
                using var sw = new StreamWriter(new FileStream(SettingsPath, FileMode.Create, FileAccess.Write));
                sw.Write(JsonSerializer.Serialize(Settings));
            }
            catch { }
        }

        private void GetSettings()
        {
            if (File.Exists(SettingsPath))
            {
                try
                {
                    Settings = JsonSerializer.Deserialize<AppSettings>(File.ReadAllText(SettingsPath));
                }
                catch
                {
                    Settings = new AppSettings();
                }
            }
            else
            {
                Settings = new AppSettings();
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            _styleManager = new StyleManager(this);
            GetSettings();
            _viewModel = new ViewModels.MainWindowViewModel(Settings);
            DataContext = _viewModel;
            Position = new PixelPoint(Settings.X, Settings.Y);
            Width = Settings.Width;
            Height = Settings.Height;
            WindowState = Settings.WindowState;
            Title += $" v{Properties.Resources.Version}";
            GotFocus += (s, e) =>
            {
                try
                {
                    if (_loadingWindow != null)
                    {
                        _loadingWindow.Topmost = true;
                        _loadingWindow.Topmost = false;
                    }
                }
                catch { }
            };
            PositionChanged += (s, e) =>
            {
                try
                {
                    Settings.X = e.Point.X;
                    Settings.Y = e.Point.Y;
                    if (_loadingWindow != null)
                        _loadingWindow.Position = new PixelPoint(PositionLoadingWindowX, PositionLoadingWindowY);
                }
                catch { }
            };
            LayoutUpdated += (s, e) =>
            {
                try
                {
                    if (_loadingWindow != null && _loadingWindow.Width != LoadingWindowWidth)
                        _loadingWindow.Width = LoadingWindowWidth;
                    if (_loadingWindow != null && _loadingWindow.Position != new PixelPoint(PositionLoadingWindowX, PositionLoadingWindowY))
                        _loadingWindow.Position = new PixelPoint(PositionLoadingWindowX, PositionLoadingWindowY);
                }
                catch { }
            };
            MessageVisualizer.TextEditor = TextEditor;
            TextEditor.LayoutUpdated += MessageVisualizer.TextEditor_LayoutUpdated;
            MessageOutputManager.MessageCountChanged += (n) => _viewModel.MessageCount = n.ToString();
        }

        public void ButtonAbout_Click(object sender, RoutedEventArgs e)
        {
            _aboutWindow = new AboutWindow();
            _aboutWindow.Show();
        }

        public void Tb_KeyUp(object sender, Avalonia.Input.KeyEventArgs e)
        {
            if (e.Key == Avalonia.Input.Key.Enter)
            {
                ChangeStringFilter((TextBox)sender, null);
            }
        }

        private void SetFiltresFromTb(TextBox tb)
        {
            switch (tb.Name.Remove(0, 7))
            {
                case "SearchDevice":
                    MessageOutputManager.SearchDevice = tb.Text;
                    break;
                case "Key":
                    MessageOutputManager.SearchKey = tb.Text;
                    break;
                case "Value":
                    MessageOutputManager.SearchValue = tb.Text;
                    break;
                case "SearchLine":
                    MessageOutputManager.SearchLine = tb.Text;
                    break;
            }
        }

        public void ChangeStringFilter(object sender, RoutedEventArgs args)
        {
            var tb = (TextBox)sender;
            if (tb.Name.Contains("Date"))
            {
                if (IsDateTime.IsMatch(tb.Text))
                {
                    if (tb.Name.Contains("Start"))
                    {
                        MessageOutputManager.StartDate = StartDate;
                    }
                    else
                    {
                        MessageOutputManager.EndDate = EndDate;
                    }
                }
                else
                {
                    if (MessagesCount > 0)
                    {
                        if (tb.Name.Contains("Start"))
                            tb.Text = MessageOutputManager.Stash[0].Time.ToString(DateFormat);
                        else
                            tb.Text = MessageOutputManager.Stash.Last().Time.ToString(DateFormat);
                    }
                }
            }
            else
            {
                SetFiltresFromTb(tb);
            }
        }

        public void ChangeTypeFilter(object sender, RoutedEventArgs args)
        {
            var cb = (CheckBox)sender;
            switch (cb.Name.Remove(0, 8))
            {
                case "Message":
                    MessageOutputManager.AreThereMesages = cb.IsChecked ?? false;
                    break;
                case "Error":
                    MessageOutputManager.AreThereErrors = cb.IsChecked ?? false;
                    break;
                case "Warning":
                    MessageOutputManager.AreThereWarnings = cb.IsChecked ?? false;
                    break;
                case "Critical":
                    MessageOutputManager.AreThereCriticals = cb.IsChecked ?? false;
                    break;
            }
        }

        private void FixScreen()
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
                WindowState = WindowState.Maximized;
            }
            else if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Maximized;
                WindowState = WindowState.Normal;
            }
        }

        public void MainWindow_Closed(object sender, EventArgs e)
        {
            Settings.Width = Width;
            Settings.Height = Height;
            Settings.WindowState = WindowState;
            WriteSettings();
            if (_settingsWindow != null)
                _settingsWindow.Close();
            if (_loadingWindow != null)
                _loadingWindow.Close();
            if (_aboutWindow != null)
                _aboutWindow.Close();
        }

        public async void OpenFileDialog(object sender, RoutedEventArgs e)
        {
            var res = await new OpenFileDialog()
            {
                Title = "�������� ���������",
                AllowMultiple = true
            }.ShowAsync(GetWindow());
            if (res != null)
                ChangeFileList(res);
        }

        public async void Settings_Click(object sender, RoutedEventArgs e)
        {
            if (_settingsWindow == null)
            {
                _settingsWindow = new SettingsWindow(Settings);
                _settingsWindow.Show();
                Settings = await _settingsWindow.GetSettingsAsync();
                _settingsWindow = null;
                if (
                    !_viewModel.FontFamily.Name.Equals(Settings.FontFamily) ||
                    _viewModel.FontSize != Settings.FontSize ||
                    StyleManager.CurrentTheme != Settings.Theme
                    )
                    FixScreen();
                _viewModel.AppSettings = Settings;
                WriteSettings();
            }
        }

        private static int ToInt(string line)
        {
            StringBuilder sb = new StringBuilder();
            bool delete = true;
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == '0')
                {
                    if (!delete)
                    {
                        sb.Append(line[i]);
                    }
                }
                if (line[i] != '0')
                {
                    delete = false;
                    sb.Append(line[i]);
                }
            }
            if (sb.Length == 0)
                sb.Append('0');
            return Convert.ToInt32(sb.ToString());
        }

        public static DateTime? GetDateTimeFromString(string datetime)
        {
            var dateandtime = datetime.Split(new char[] { '.', ':', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                return dateandtime.Length switch
                {
                    6 => new DateTime(
                        ToInt(dateandtime[2]),
                        ToInt(dateandtime[1]),
                        ToInt(dateandtime[0]),
                        ToInt(dateandtime[3]),
                        ToInt(dateandtime[4]),
                        ToInt(dateandtime[5])
                        ),
                    _ => null
                };
            }
            catch
            {
                return null;
            }
        }

        private async void ChangeFileList(string[] names)
        {
            if (names.Length > 0)
            {
                if (_cancellationToken != null)
                {
                    _cancellationToken.Cancel();
                    await Task.Run(() =>
                    {
                        while (_cancellationToken != null)
                            Thread.Sleep(150);
                    });

                    _textParser = new TextParser();
                    _cancellationToken = new CancellationTokenSource();
                    _loadingWindow.Close();
                }

                var list = new List<IMessage>();

                _loadingWindow = new LoadingWindow()
                {
                    Width = LoadingWindowWidth,
                    Height = LoadingWindowHeight,
                    Position = new PixelPoint(PositionLoadingWindowX, PositionLoadingWindowY),
                    Background = TextEditor.Background,
                    Topmost = true,
                    Maximum = 100 * names.Length,
                    Value = 0,
                };
                _loadingWindow.GotFocus += (s, e) =>
                {
                    Focus();
                };
                _loadingWindow.Show();
                _loadingWindow.Topmost = false;

                TextParser.ValueChanged += UpdateValue;

                _cancellationToken = new CancellationTokenSource();

                foreach (var name in names)
                {
                    list.AddRange(await _textParser.ParseToMessageArrayAsync(File.ReadAllText(name), _cancellationToken));
                }

                _cancellationToken = null;

                TextParser.ValueChanged -= UpdateValue;

                if (list.Count > 0)
                {
                    MessageOutputManager.IsWritingBlocked = true;
                    MessageOutputManager.StartDate = list[0].Time;
                    MessageOutputManager.EndDate = list.Last().Time;
                    MessageOutputManager.Stash = list.ToArray();
                    MessageOutputManager.IsWritingBlocked = false;
                    TextBoxStartDate.Text = list[0].Time.ToString(DateFormat);
                    TextBoxEndDate.Text = list.Last().Time.ToString(DateFormat);
                }

                if (_loadingWindow != null)
                    _loadingWindow.Close();
            }
        }

        private void UpdateValue()
        {
            _loadingWindow.Value++;
        }
    }

}