using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using SlpBsLogAnalyzer.Styles;

namespace SlpBsLogAnalyzer.Views
{
    public class AboutWindow : Window
    {
        internal class ViewModel : ViewModels.ViewModelBase
        {
            public string Page
            {
                get => @"https://andrewmat2000@bitbucket.org/andrewmat2000/adsvelfirmwareupdater.git";
                set { }
            }
        }

        public AboutWindow()
        {
            InitializeComponent();
            DataContext = new ViewModel();
            new StyleManager(this).UseTheme(StyleManager.CurrentTheme);
        }

        private void InitializeComponent() => AvaloniaXamlLoader.Load(this);


    }
}