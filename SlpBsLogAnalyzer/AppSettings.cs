﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Avalonia.Controls;
using ReactiveUI;

namespace SlpBsLogAnalyzer
{
    /// <summary>
    /// Класс с настройками приложения для сериализации/десериализации json.
    /// </summary>

    public class AppSettings
    {
        public WindowState WindowState { get; set; }

        public static AppSettings Current { get; set; }

        public Styles.Themes Theme { get; set; }

        public string FontFamily
        {
            get => _fontFamily;
            set => _fontFamily = value;
        }

        private string _fontFamily;

        public double FontSize
        {
            get => _fontSize;
            set => _fontSize = value;
        }

        private double _fontSize;

        public int MessageCount { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public double Height { get; set; }

        public double Width { get; set; }

        public AppSettings()
        {
            WindowState = WindowState.Normal;
            FontFamily = "Consolas";
            Theme = Styles.Themes.Dark;
            FontSize = 14;
            MessageCount = 50;
            Height = 450;
            Width = 1100;
            X = 0;
            Y = 0;
        }

        public AppSettings Clone() => new AppSettings
        {
            WindowState = WindowState,
            FontFamily = FontFamily,
            FontSize = FontSize,
            Height = Height,
            MessageCount = MessageCount,
            Theme = Theme,
            Width = Width,
            X = X,
            Y = Y
        };

    }
}
