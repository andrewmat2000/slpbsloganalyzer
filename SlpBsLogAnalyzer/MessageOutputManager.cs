﻿using Avalonia.Platform;
using Avalonia.Threading;
using AvaloniaEdit;
using AvaloniaEdit.Document;
using DynamicData;
using SlpBsLogAnalyzer.Messages;
using SlpBsLogAnalyzer.Painters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SlpBsLogAnalyzer
{

    /// <summary>
    /// Класс, проводящий фильтрацию сообщений по фильтрам.
    /// </summary>

    public static class MessageOutputManager
    {
        #region Поля

        private static IMessage[] _stash = new IMessage[0];

        private static string _searchKey = "";

        private static DateTime _startDate;

        private static DateTime _endDate;

        private static string _searchValue = "";

        private static string _searchDevice = "";

        private static string _searchLine = "";

        private static bool _areThereMesages = true;

        private static bool _areThereErrors = true;

        private static bool _areThereWarnings = true;

        private static bool _areThereCriticals = true;

        public delegate void MessageCountEvent(int newCount);

        public static event MessageCountEvent MessageCountChanged;

        private static readonly List<int> _availableIds = new List<int>();

        #endregion

        #region Свойства

        private static Regex IsNumber => new Regex(@"\d+");

        public static int OptimalMessageCount => 150;

        private static string Id => "id";

        public static bool IsWritingBlocked { get; set; }

        public static IMessage[] Stash
        {
            set
            {
                _stash = value;
                _availableIds.Clear();
                foreach (var message in value)
                {
                    dynamic messageObject = message.SearchByLine(Id, typeof(MessageObject));
                    if (messageObject is MessageObject)
                    {
                        var id = messageObject.Id;
                        if (id != null && !_availableIds.Contains(id))
                        {
                            _availableIds.Add(id);
                        }
                    }
                }
                _availableIds.Sort();
                ShowAll();
            }
            get => _stash;
        }

        #endregion

        private static bool IsBool(dynamic obj) => obj.GetType().Equals(typeof(bool));

        private static bool CheckType(IMessage mes)
            =>
            (!_areThereCriticals && !_areThereErrors && !_areThereMesages && !_areThereWarnings) ||
            mes.Type switch
            {
                "warning" => _areThereWarnings,
                "error" => _areThereErrors,
                "critical" => _areThereCriticals,
                _ => _areThereMesages
            };

        private static bool CheckDate(IMessage message) => message.Time >= _startDate && message.Time <= _endDate;

        private static bool CheckKeyValue(IMessage message)
        {
            if (_searchValue.Length == 0 && _searchKey.Length == 0)
                return true;
            return !message.SearchByLine($"{_searchKey}: {_searchValue}", typeof(MessageObject),typeof(KeyValuePair<string, string>)).GetType().Equals(typeof(bool));
        }

        private static bool CheckDevice(IMessage message, params int[] ids)
        {
            if (ids != null && ids.Length > 0)
                foreach (var id in ids)
                {
                    if (message.SearchByLine(
                        $"id: {id}",
                        typeof(MessageObject)) is MessageObject device &&
                        device.Id != null && device.Id == id
                        )
                        return true;
                }
            return false;
        }

        private static bool CheckDevice(string line, IMessage message)
        {
            if (line.Length == 0)
                return true;
            if (line.Contains(",") || line.Contains("-"))
            {
                var list = new List<int>();
                var lines = line.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (lines.Length > 0)
                    for (var i = 0; i < lines.Length; i++)
                    {
                        if (lines[i].Contains("-"))
                        {
                            var numbers = lines[i].Split('-');
                            if (IsNumber.IsMatch(numbers[0]) && IsNumber.IsMatch(numbers[1]))
                            {
                                var n1 = Convert.ToInt32(numbers[0]);
                                var n2 = Convert.ToInt32(numbers[1]);
                                int max = n1;
                                int min = n2;
                                if (n1 < n2)
                                {
                                    max = n2;
                                    min = n1;
                                }
                                if (max >= _availableIds[0] && min <= _availableIds.Last())
                                    for (var j = min; j <= max; j++)
                                    {
                                        if (_availableIds.Contains(j))
                                            list.Add(j);
                                    }
                            }
                        }
                        else
                        {
                            if (int.TryParse(lines[i], out int result))
                                if (result >= _availableIds[0] && result <= _availableIds.Last())
                                    list.Add(result);
                        }
                    }
                else
                {
                    return false;
                }
                if (CheckDevice(message, list.ToArray()))
                    return true;
            }
            else
            {
                if (int.TryParse(line, out int result))
                    return CheckDevice(message, result);
                return false;
            }
            return false;
        }

        private static bool CheckLine(IMessage message) =>
            _searchLine.Length == 0 || message.ToString().Contains(_searchLine);


        private static CancellationTokenSource cancellation;

        private async static void MakeToShowArrayAsync()
        {
            MessageVisualizer.ClearAll();

            if (cancellation != null)
            {
                cancellation.Cancel();
                await Task.Run(() =>
                {
                    while (cancellation != null)
                        Thread.Sleep(15);
                });
            }

            cancellation = new CancellationTokenSource();

            await Task.Run(() =>
            {
                var isEnough = false;
                var list = new List<IMessage>();
                for (var i = 0; i < _stash.Length; i++)
                {
                    if (cancellation.IsCancellationRequested)
                    {
                        break;
                    }
                    if (
                        CheckType(_stash[i]) &&
                        CheckDate(_stash[i]) &&
                        CheckKeyValue(_stash[i]) &&
                        CheckDevice(_searchDevice, _stash[i]) &&
                        CheckLine(_stash[i])
                        )
                        list.Add(_stash[i]);
                    if (!isEnough && list.Count - 100 > MessageVisualizer.MaxCount)
                    {
                        MessageVisualizer.AddRange(list.ToArray());
                        list.Clear();
                        isEnough = true;
                    }
                }
                MessageVisualizer.AddRange(list.ToArray());
                MessageCountChanged(MessageVisualizer.MessageCount);
            });

            cancellation = null;

        }

        private static void ShowAll()
        {
            MakeToShowArrayAsync();
        }


        public static DateTime StartDate
        {
            set
            {
                if (_startDate != value)
                {
                    _startDate = value;
                    if (!IsWritingBlocked)
                        ShowAll();
                }
            }
            get => _startDate;
        }

        public static DateTime EndDate
        {
            set
            {
                if (_endDate != value)
                {
                    _endDate = value;
                    if (!IsWritingBlocked)
                        ShowAll();
                }
            }
            get => _endDate;
        }

        public static bool AreThereMesages
        {
            set
            {
                if (_areThereMesages != value)
                {
                    _areThereMesages = value;
                    ShowAll();
                }
            }
        }

        public static bool AreThereErrors
        {
            set
            {
                if (_areThereErrors != value)
                {
                    _areThereErrors = value;
                    ShowAll();
                }
            }
        }

        public static bool AreThereWarnings
        {
            set
            {
                if (_areThereWarnings != value)
                {
                    _areThereWarnings = value;
                    ShowAll();
                }
            }
        }

        public static bool AreThereCriticals
        {
            set
            {
                if (_areThereCriticals != value)
                {
                    _areThereCriticals = value;
                    ShowAll();
                }
            }
        }

        public static string SearchKey
        {
            set
            {
                if (!value.Equals(_searchKey))
                {
                    _searchKey = value;
                    ShowAll();
                }
            }
        }

        public static string SearchValue
        {
            set
            {
                if (!value.Equals(_searchValue))
                {
                    _searchValue = value;
                    ShowAll();
                }
            }
        }

        public static string SearchDevice
        {
            set
            {
                if (!value.Equals(_searchDevice))
                {
                    _searchDevice = value;
                    ShowAll();
                }
            }
        }
        public static string SearchLine
        {
            set
            {
                if (!value.Equals(_searchLine))
                {
                    _searchLine = value;
                    ShowAll();
                }
            }
        }

    }
}
