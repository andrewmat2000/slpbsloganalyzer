﻿using Avalonia.Controls;
using Avalonia.Markup.Xaml.Styling;
using Avalonia.Media;
using AvaloniaEdit;
using SlpBsLogAnalyzer.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace SlpBsLogAnalyzer.Styles
{

    public enum Themes
    {
        Normal,
        Dark
    }

    /// <summary>
    /// Класс с помощью, которого осуществялется работа с темами.
    /// </summary>
    public sealed class StyleManager
    {

        private readonly StyleInclude Dark = CreateStyle("avares://SlpBsLogAnalyzer/Styles/Dark.xaml");
        
        private readonly StyleInclude Normal = CreateStyle("avares://SlpBsLogAnalyzer/Styles/Normal.xaml");

        private readonly Window _window;

        public delegate void ThemeChangedHandler();

        public static event ThemeChangedHandler ThemeChanged;

        public StyleManager(Window window)
        {
            _window = window;

            // We add the style to the window styles section, so it
            // will override the default style defined in App.xaml. 
            if (window.Styles.Count == 0)
                window.Styles.Add(Normal);

            // If there are styles defined already, we assume that
            // the first style imported it related to citrus.
            // This allows one to override citrus styles.
            else window.Styles[0] = Normal;
        }

        public static Themes CurrentTheme
        {
            get => _currentTheme;
            private set
            {
                if (value != _currentTheme)
                {
                    _currentTheme = value;
                    ThemeChanged?.Invoke();
                }
            }
        }

        private static Themes _currentTheme = Themes.Dark;

        public void UseTheme(Themes theme)
        {
            // Here, we change the first style in the main window styles
            // section, and the main window instantly refreshes. Remember
            // to invoke such methods from the UI thread.
            _window.Styles[0] = theme switch
            {
                Themes.Normal => Normal,
                Themes.Dark => Dark,
                _ => throw new ArgumentOutOfRangeException(nameof(theme))
            };
            CurrentTheme = theme;
        }

        private static StyleInclude CreateStyle(string url)
        {
            var self = new Uri("resm:Styles?assembly=Citrus.Avalonia.Sandbox");
            return new StyleInclude(self)
            {
                Source = new Uri(url)
            };
        }
    }
}
