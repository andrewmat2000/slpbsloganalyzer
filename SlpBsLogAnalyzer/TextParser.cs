﻿using Avalonia.Controls.Shapes;
using Avalonia.Input;
//using DynamicData;
//using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reactive.Joins;
using System.Reflection.Metadata;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using SlpBsLogAnalyzer.Messages;
using System.Runtime.InteropServices.ComTypes;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace SlpBsLogAnalyzer
{

    /// <summary>
    /// Статический класс, предзназначеный для парсинга строк в объекты интерфейса IMessage.
    /// </summary>

    public class TextParser
    {
        #region Свойства

        private List<IMessage> _messages = new List<IMessage>();

        private MessageObject _currentDevice;

        private DynamicList _currentTree;

        private bool _isSANR = false;

        private DateTime _lastNormalDateTime;

        public delegate void UpdateValue();

        public static UpdateValue ValueChanged;

        #endregion

        #region Поля

        private static Regex IsMessageType => new Regex(@"\[[A-Z]+\]");

        private static Regex IsSu => new Regex(@"[A-z]+\s\d+:");

        private static Regex IsKeyValuePair => new Regex(@"([a-z]|[_]){2,}:\s(([0-9]|[.]|\s|[a-f]|[-])+|(true)|(false)|(null))$", RegexOptions.IgnoreCase);

        private static Regex EmptySANR => new Regex(@"=*\sSTART\sA\sNEW\sRECORD\s=*\n=*\n", RegexOptions.Singleline | RegexOptions.IgnoreCase);

        private static Regex SplitSeparators => new Regex(@"\s{2}-\s{2}");

        private static Regex SplitWhiteSpacesAndTabs => new Regex(@"\s{0,}\t+");

        private static Regex IsMessage => new Regex(@"\[\d\d\d\d\.\d\d\.\d\d\s\d\d\:\d\d\:\d\d\..+\]", RegexOptions.Singleline | RegexOptions.IgnoreCase);

        private string Id => "id";

        #endregion

        private bool IsCasualMessage(string Text) => !Text.StartsWith("======") && !Text.StartsWith("//");

        private bool IsSTARTANEWRECORDMESSAGE(string Text) => Text.Contains("START A NEW RECORD");

        private bool IsBigParametresData(string Text) => Text.Contains("====") && !Text.Contains("START A NEW RECORD");

        private int TryParseToInt32(string a)
        {
            try
            {
                return Convert.ToInt32(a);
            }
            catch
            {
                return 0;
            }
        }

        private int ToInt(string line)
        {
            StringBuilder sb = new StringBuilder();
            bool delete = true;
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == '0')
                {
                    if (!delete)
                    {
                        sb.Append(line[i]);
                    }
                }
                if (line[i] != '0')
                {
                    delete = false;
                    sb.Append(line[i]);
                }
            }
            if (sb.Length == 0)
                sb.Append('0');
            return TryParseToInt32(sb.ToString());
        }

        public DateTime? GetDateTimeFromString(string datetime)
        {
            var dateandtime = datetime.Trim(new char[] { '[', ']' }).Split(new char[] { '.', ':' }, StringSplitOptions.RemoveEmptyEntries);
            try
            {
                return new DateTime(
                    ToInt(dateandtime[0]),
                    ToInt(dateandtime[1]),
                    ToInt(dateandtime[2]),
                    ToInt(dateandtime[3]),
                    ToInt(dateandtime[4]),
                    ToInt(dateandtime[5]),
                    ToInt(dateandtime[6])
                    );
            }
            catch
            {
                if (_lastNormalDateTime != null)
                    return _lastNormalDateTime;
                return null;
            }
        }

        private int StringDateToInt(string text, bool isDay)
        {
            if (isDay)
            {
                return text switch
                {
                    "Tue" => 2,
                    "Wed" => 3,
                    "Thu" => 4,
                    "Fri" => 5,
                    "Sat" => 6,
                    "Sun" => 7,
                    _ => 1,
                };
            }
            else
            {
                return text switch
                {
                    "Feb" => 2,
                    "Mar" => 3,
                    "Apr" => 4,
                    "May" => 5,
                    "Jun" => 6,
                    "Jul" => 7,
                    "Aug" => 8,
                    "Sep" => 9,
                    "Oct" => 10,
                    "Nov" => 11,
                    "Dec" => 12,
                    _ => 1,
                };
            }
        }

        private Parametres ParseParamentresMessage(string text)
        {
            var allines = text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            var parametres = new Parametres();
            for (int i = 2; i < allines.Length - 1; i++)
            {
                var lines = SplitWhiteSpacesAndTabs.Split(allines[i].TrimStart(new char[] { '/', ' ' }));
                parametres.Data.Add(new KeyValuePair<string, string>(lines[0], lines[1]));
            }
            parametres.Date = allines[1];
            return parametres;
        }

        private Message ParseCasualMessage(string text)
        {
            var isDevice = false;
            _currentDevice = null;
            var allines = text.Split('\n');
            var fline = allines[0].Split(' ');
            var time = GetDateTimeFromString($"{fline[0]}.{fline[1]}");
            var type = IMessage.WithoutType;
            var sb = new StringBuilder();
            for (var i = 2; i < fline.Length; i++)
            {
                if (IsMessageType.IsMatch(fline[i]))
                {
                    type = fline[i].Trim(new char[] { '[', ']' }).ToLower();
                }
                else
                {
                    sb.Append(fline[i] + ' ');
                }
            }
            if (sb.ToString().Last() == ' ')
                sb.Remove(sb.Length - 1, 1);
            var data = new DynamicList(sb.ToString().Trim('\r'));
            _currentTree = data;
            sb.Clear();
            for (var i = 1; i < allines.Length; i++)
            {
                var thisline = allines[i].Trim(new char[] { '\t', '\r' });
                if (thisline.Contains("{") && !thisline.Contains("}"))
                {
                    _currentTree.Add(new DynamicList(thisline));
                    _currentTree = _currentTree.Children.Last();
                }
                else if (thisline.Contains("}") && !thisline.Contains("{"))
                {
                    if (_currentTree.HasParent)
                    {
                        _currentTree = _currentTree.Parent;
                    }
                    _currentTree.Add(thisline);
                }
                else if (thisline.Contains("{") && thisline.Contains("}"))
                {
                    var array = thisline.Split(':');
                    var mesobj = new MessageObject($"{{{array[0]}}}");
                    mesobj.Attributes.Add(array[0], array[1].TrimStart(' ').Split(new char[] { '{', '}' }, StringSplitOptions.RemoveEmptyEntries)[0].TrimEnd(' '));
                    _currentTree.Add(mesobj);
                }
                else if (allines.Length > i + 1 && SplitSeparators.IsMatch(allines[i + 1]) && !isDevice)
                {
                    isDevice = true;
                    if (IsSu.IsMatch(thisline))
                        _currentDevice = new MessageObject(thisline.TrimEnd(new char[] { ' ', ':' }));
                    else
                    {
                        _currentDevice = new MessageObject(allines[i - 1].Trim(new char[] { '\t', '\r' }));
                        i--;
                    }
                }
                else if (isDevice)
                {
                    if (IsSu.IsMatch(thisline))
                    {
                        isDevice = false;
                        _currentTree.Add(_currentDevice);
                        _currentDevice = null;
                        i--;
                    }
                    else
                    {
                        foreach (var obj in SplitSeparators.Split(thisline))
                        {
                            var keyvalue = obj.Split(':');
                            for (var j = 1; j < keyvalue.Length; j++)
                            {
                                sb.Append(keyvalue[j]);
                                if (j != keyvalue.Length - 1)
                                    sb.Append(":");
                            }
                            if (!_currentDevice.Attributes.ContainsKey(keyvalue[0]))
                                _currentDevice.Attributes.Add(keyvalue[0], sb.ToString().TrimStart(' ').TrimEnd('\r'));
                            sb.Clear();
                        }
                        _currentDevice.EndLinesIndexes.Add(_currentDevice.Attributes.Count - 1);
                        if (allines.Length > i + 1 && allines[i + 1].Contains('}'))
                        {
                            isDevice = false;
                            _currentTree.Add(_currentDevice);
                            _currentDevice = null;
                        }
                    }
                }
                else
                {
                    if (IsKeyValuePair.IsMatch(thisline))
                    {
                        var keyvalue = thisline.Split(':');
                        _currentTree.Add(new KeyValuePair<string, string>(keyvalue[0], keyvalue[1].TrimStart(' ')));
                    }
                    else
                    {
                        _currentTree.Add(thisline);
                    }
                }
            }
            _currentTree = null;
            if (time != null)
                return new Message((DateTime)time, type, data);
            return Message.GetEmptyMessage();
        }

        private SANR STARTANEWRECORD(string Text)
        {
            var thismessage = new SANR();
            if (!EmptySANR.IsMatch(Text))
            {
                foreach (var a in Text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (!a.Contains("===="))
                    {
                        var message = ParseToMessage(a);
                        _messages.Add(message);
                        if (thismessage.Time.Year == 0001)
                        {
                            thismessage.Time = message.Time;
                        }
                    }
                }
            }
            else
            {
                if (Text.Contains("//"))
                {
                    var sb = new StringBuilder();
                    var mas = Text.Split('\n');
                    for (int i = 2; i < mas.Length; i++)
                        sb.Append(mas[i] + '\n');
                    _messages.Add(ParseParamentresMessage("\n=====" + sb.ToString()));
                    thismessage.Time = _messages.Last().Time;
                }
            }
            return thismessage;
        }

        private IMessage ParseToMessage(string Text)
        {
            IMessage message = new EmptyMessage();
            if (IsCasualMessage(Text))
            {
                message = ParseCasualMessage(Text.TrimEnd('\n'));
            }
            else if (IsSTARTANEWRECORDMESSAGE(Text))
            {
                message = STARTANEWRECORD(Text);
            }
            else if (IsBigParametresData(Text))
            {
                message = ParseParamentresMessage(Text);
            }
            if (!message.Type.Equals("empty"))
                _lastNormalDateTime = message.Time;
            return message;
        }


        public async Task<IMessage[]> ParseToMessageArrayAsync(string Text, CancellationTokenSource tokenSource) => await Task.Run(() =>
       {
           _messages = new List<IMessage>();
           var lines = Text.Split('\n');
           var sb = new StringBuilder();
           var lastnumber = 0;
           for (var i = 0; i < lines.Length; i++)
           {
               if ((IsMessage.IsMatch(lines[i]) || IsBigParametresData(lines[i]) || IsSTARTANEWRECORDMESSAGE(lines[i])) && sb.Length > 0)
               {
                   _messages.Add(ParseToMessage(sb.ToString()));
                   if (_isSANR)
                   {
                       _messages[^2].Time = _messages.Last().Time;
                       _isSANR = false;
                   }
                   if (IsSTARTANEWRECORDMESSAGE(sb.ToString()))
                       _isSANR = true;
                   sb.Clear();
               }
               if (!lines[i].Equals("\r") && lines[i].Length > 0)
               {
                   sb.Append(lines[i].Trim('\r') + '\n');
                   if (sb.ToString()[^2] == ' ')
                       sb.Remove(sb.Length - 2, 1);
               }
               if (i / (double)lines.Length * 100 - lastnumber > 1)
               {
                   lastnumber = (int)Math.Round(i / (double)lines.Length * 100);
                   ValueChanged?.Invoke();
               }
               if (tokenSource.Token.IsCancellationRequested)
                   return _messages.ToArray();
           }
           if (sb.Length > 0)
               _messages.Add(ParseToMessage(sb.ToString()));
           if (_messages.Last() as SANR != null)
           {
               for (var i = 1; i < _messages.Count - 1; i++)
                   if (_messages[^i] as SANR == null)
                       _messages.Last().Data = _messages[^i].Data;
           }
           return _messages.ToArray();
       });

    }
}