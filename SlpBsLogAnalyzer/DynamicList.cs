﻿using Avalonia.Data.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace SlpBsLogAnalyzer
{

    /// <summary>
    /// Простое дерево для запоминания и вывода данных.
    /// "Dynamic" так как хранит в себе в себе тип dynamic.
    /// </summary>

    public class DynamicList
    {

        public readonly List<DynamicList> Children = new List<DynamicList>();

        private dynamic _value;

        public bool HasParent => Parent != null;

        private string _line = "";

        public DynamicList Parent { get; private set; }

        private DynamicList(DynamicList parent) { Parent = parent; }

        public DynamicList(dynamic Value) { _value = Value; }

        public string Value => GetDynamicToString(_value);

        public bool IsDevice => _value as MessageObject != null;

        public int KeyLength => _value.Key.Length;

        public int ValueLength => _value.Value.Length;

        public DynamicList this[int ind] => Children[ind];

        public bool IsKeyValuePair
        {
            get
            {
                try
                {
                    KeyValuePair<string, string> a = _value;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool IsThereDevice
        {
            get
            {
                foreach (DynamicList tree in Children)
                {
                    if (tree.IsDevice)
                        return true;
                }
                return false;
            }
        }


        public void Add(dynamic obj) => Children.Add(new DynamicList(this) { _value = obj });

        private static bool IsTrueType(dynamic obj, params Type[] types)
        {
            foreach (var type in types)
            {
                if (obj.GetType().Equals(type))
                    return true;
            }
            return false;
        }

        public dynamic Search(string line, params Type[] types)
        {
            if (Value.Contains(line) && IsTrueType(_value, types))
            {
                return this;
            }
            return Search(this, line, types);
        }

        private bool CheckKeyValuePair(dynamic value, string line, Type[] types)
        {
            if (value.GetType() != typeof(KeyValuePair<string, string>) && value.GetType() != typeof(MessageObject))
                return true;
            else if (value.GetType() == typeof(MessageObject))
            {
                var hasKeyValue = false;
                foreach (var type in types)
                {
                    if (type == typeof(KeyValuePair<string, string>))
                        hasKeyValue = true;
                }
                if (!hasKeyValue)
                    return true;
                else
                {
                    Dictionary<string, string> dictionary = value.Attributes;
                    for (var i = 0; i < dictionary.Count; i++)
                    {
                        if (dictionary.ElementAt(i).Key.Equals(line.Split(':')[0]))
                            return CheckKeyValuePair(dictionary.ElementAt(i), line, new Type[] { typeof(KeyValuePair<string, string>) });
                    }
                    return false;
                }
            }
            else
            {
                var lines = line.Split(new char[] { ':', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                switch (lines.Length)
                {
                    case 1:
                        if (lines[0].Equals(value.Key))
                            return true;
                        break;
                    case 2:
                        if (lines[0].Equals(value.Key) && lines[1].Equals(value.Value))
                            return true;
                        break;
                    default:
                        var sb = new StringBuilder();
                        for (var i = 1; i < lines.Length; i++)
                        {
                            sb.Append(lines[i]);
                            if (i != lines.Length - 1)
                                sb.Append(' ');
                        }
                        if (lines[0].Equals(value.Key) && sb.ToString().Equals(value.Value))
                            return true;
                        break;
                }
                return false;
            }
        }

        private dynamic Search(DynamicList tree, string line, params Type[] types)
        {
            if (tree.Children.Count != 0)
                foreach (DynamicList child in tree.Children)
                {
                    var result = child.Search(child, line, types);

                    if (!result.GetType().Equals(typeof(bool)) && IsTrueType(result, types))
                    {
                        if (CheckKeyValuePair(result, line, types))
                            return result;
                    }
                }
            else
            {
                if (GetDynamicToString(tree._value).Contains(line) && IsTrueType(tree._value, types))
                {
                    if (CheckKeyValuePair(tree._value, line, types))
                        return tree._value;
                }
            }
            return false;
        }

        private string GetDynamicToString(dynamic value)
        {
            string line;
            if (value as string != null)
            {
                line = $"\t{value}\n";
            }
            else if (value as DynamicList != null)
            {
                line = value.ToString();
            }
            else if (value as MessageObject != null)
            {
                line = value.ToString();
            }
            else
            {
                var pair = (KeyValuePair<string, string>)value;
                line = $"\t{pair.Key}: {pair.Value}\n";
            }
            return line;
        }

        private string Build(DynamicList tree)
        {
            if (tree.Children.Count > 0)
            {
                foreach (DynamicList child in tree.Children)
                {
                    _line += child.ToString();
                }
            }
            return _line;
        }

        public override string ToString()
        {
            _line = Value;
            return Build(this);
        }

    }
}
