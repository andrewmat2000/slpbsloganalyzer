﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SlpBsLogAnalyzer.Messages
{
    public interface IMessage : IComparable
    {
        public dynamic SearchByLine(string line, params Type[] type) => Data.Search(line, type);

        public DynamicList this[int ind] { get => Data[ind]; }

        public DateTime Time { get; set; }

        public DynamicList Data { get; set; }

        public string Type { get; set; }

        public static string WithoutType => "message";

        public static string BPM => "BigParamentresMessage";

        public string ToString();

        public int LineCount { get { return ToString().Split('\n').Length; } }
    }
}
