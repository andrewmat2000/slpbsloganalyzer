﻿using AvaloniaEdit.CodeCompletion;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SlpBsLogAnalyzer.Messages
{
    class Parametres : IMessage
    {
        private static int TryParse(string line)
        {
            try
            {
                return Convert.ToInt32(line);
            }
            catch
            {
                return 0;
            }
        }

        private static int ToInt(string line)
        {
            StringBuilder sb = new StringBuilder();
            bool delete = true;
            for (int i = 0; i < line.Length; i++)
            {
                if (line[i] == '0')
                {
                    if (!delete)
                    {
                        sb.Append(line[i]);
                    }
                }
                if (line[i] != '0')
                {
                    delete = false;
                    sb.Append(line[i]);
                }
            }
            if (sb.Length == 0)
                sb.Append('0');
            return TryParse(sb.ToString());
        }
        private static int StringDateToInt(string text, bool isDay)
        {
            if (isDay)
            {
                return text switch
                {
                    "Mon" => 1,
                    "Tue" => 2,
                    "Wed" => 3,
                    "Thu" => 4,
                    "Fri" => 5,
                    "Sat" => 6,
                    "Sun" => 7,
                    _ => 1,
                };
            }
            else
            {
                return text switch
                {
                    "Jan" => 1,
                    "Feb" => 2,
                    "Mar" => 3,
                    "Apr" => 4,
                    "May" => 5,
                    "Jun" => 6,
                    "Jul" => 7,
                    "Aug" => 8,
                    "Sep" => 9,
                    "Oct" => 10,
                    "Nov" => 11,
                    "Dec" => 12,
                    _ => 1,
                };
            }
        }
        public DateTime Time 
        { 
            get 
            {
                var strtime = Date.Split(new char[] { ' ', ':' }, StringSplitOptions.RemoveEmptyEntries);
                return new DateTime(Convert.ToInt32(strtime[7]), StringDateToInt(strtime[2], false), StringDateToInt(strtime[1], true), ToInt(strtime[3]), ToInt(strtime[4]), ToInt(strtime[5]), ToInt(strtime[6])); ; 
            } 
            set => throw new ArgumentException(); 
        }

        public string Date { get; set; }

        public DynamicList Data { get; set; }

        public static string DefaultStart => "=====================================================================";

        private static string DefaultEnd => "-------------------------------------------------------------------------";

        private static Regex SplitPairs => new Regex(@": ");

        public string Type { get => "parametres"; set { } }

        private static int InLineSymbolsCount => 44;

        private static int TabLength => 7;

        public Parametres()
        {
            Data = new DynamicList("");
        }

        public int CompareTo(object obj)
        {
            var date = ((Message)obj).Time;
            if (Time < date)
                return -1;
            if (Time > date)
                return 1;
            return 0;
        }

        private static string AppendWhiteSpace(KeyValuePair<string, string> keyValuePair)
        {
            var sb = new StringBuilder();
            sb.Append(keyValuePair.Key);
            while (sb.Length + TabLength <= InLineSymbolsCount)
            {
                sb.Append(' ');
            }
            sb.Append($"       {keyValuePair.Value}");
            return sb.ToString();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < Data.Children.Count; i++)
            {
                var pair = SplitPairs.Split(Data.Children[i].Value);
                sb.Append($"// {AppendWhiteSpace(new KeyValuePair<string, string>(pair[0].Trim('\t'), pair[1].Trim('\n')))}");
                if (i != Data.Children.Count - 1)
                    sb.Append("\n");
            }
            return $"{DefaultStart}\n{Date}\n{sb}\n{DefaultEnd}";
        }

    }
}
