﻿using SlpBsLogAnalyzer.Messages;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SlpBsLogAnalyzer.Messages
{
    public class Message : IMessage
    {
        public DateTime Time { get; set; }

        public string Type { get; set; }

        private readonly StringBuilder _sb = new StringBuilder();

        public DynamicList Data { get; set; }

        private bool _hasDevice = false;

        public static Message GetEmptyMessage()
        {
            return new Message { Type = "empty" };
        }

        public int CompareTo(object obj)
        {
            var date = ((Message)obj).Time;
            if (Time < date)
                return -1;
            if (Time > date)
                return 1;
            return 0;
        }

        private Message() { }

        public Message(DateTime time, string type, DynamicList data)
        {
            Time = time;
            Type = type;
            Data = data;
        }

        public override string ToString()
        {
            _sb.Clear();
            _sb.Append($"{Time:[yyyy.MM.dd HH:mm:ss.fff]}");
            if (!Type.Equals("message"))
            {
                _sb.Append($" [{Type.ToUpper()}] ");
            }
            else
            {
                _sb.Append(' ');
            }
            _sb.Append(Data.Value.Remove(0, 1));
            foreach (var child in Data.Children)
            {
                for (var i = 0; i < child.Children.Count; i++)
                {
                    if (!_hasDevice && child[i].IsDevice)
                    {
                        _hasDevice = true;
                    }
                    if (_hasDevice && i == 0)
                    {
                        _sb.Append(child.Value);
                    }
                    if (_hasDevice)
                    {
                        _sb.Append(child[i]);
                        if (i != child.Children.Count - 1)
                            _sb.Append("\n");
                    }
                }
                if (!_hasDevice)
                    _sb.Append(child);
                _hasDevice = false;
            }
            return _sb.ToString().TrimEnd('\n');
        }

    }
}
