﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SlpBsLogAnalyzer.Messages
{
    public class SANR : IMessage
    {
        public DateTime Time { get; set; }

        public DynamicList Data { get => new DynamicList(DefaultMessage); set { } }

        public string Type { get => "sanr"; set { } }

        private static string DefaultMessage => "=========================== START A NEW RECORD ==========================";

        public int CompareTo(object obj)
        {
            var date = ((Message)obj).Time;
            if (Time < date)
                return -1;
            if (Time > date)
                return 1;
            return 0;
        }

        public override string ToString()
        {
            return DefaultMessage;
        }

    }
}
