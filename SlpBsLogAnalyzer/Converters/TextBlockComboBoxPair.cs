﻿using Avalonia.Data.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;

namespace SlpBsLogAnalyzer.Converters
{
    public class TextBlockComboBoxPair : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
        
            (double)value - 170;
        

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => value;
    }
}
